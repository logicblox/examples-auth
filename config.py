from lbconfig.api import *

lbconfig_package('example-app', version='0.1')

depends_on(
    logicblox_dep,
    lb_web_dep
)

lb_library (
    name = 'project',
    srcdir='.'
)

check_lb_workspace(
    name='lb-example',
    libraries = ['project']
)

rule('start-services', 'check-lb-workspaces', [
  '$(logicblox)/bin/lb web-server load-services -w lb-example'
], True)
