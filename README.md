Example Dataset
============

This repository contains 2 example datasets:
* The "basic" dataset can be used to learn LogiQL.
* The "extended" dataset can be used by more experienced users of LogicBlox. It contains a more complex data model together with a larger dataset. (TODO)

Explore the Dataset
==============
To build the basic sample application you simply have to run:

    $ lb config
    $ make

from the `basic` folder.

This command creates a workspace called `lb-example`, installs the library with the example schema and imports the sample data.

Schema
==============
* The calendar, location and product hierarchies are defined in the `hierarchy` folder.
* Some sample measures such as daily and monthly sales, profits, etc. are defined in the `measures` folder.
* The sample dataset itself can be found in the `data` folder.

License
==============
Copyright (C) LogicBlox, Inc. All rights reserved.
